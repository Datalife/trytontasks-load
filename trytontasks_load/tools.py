# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import os
import re
import sys
import logging
import threading

from proteus import config

__all__ = ['set_trytond', 'init_logger', 'get_logger']

CSV_DELIMITER = ';'

timedelta_regex = re.compile(
    r'((?P<hours>\d+?)hr)?((?P<minutes>\d+?)m)?((?P<seconds>\d+?)s)?')

_logger = logging.getLogger(__name__)

_CONFIG = threading.local()
_CONFIG.current = None


def set_trytond(config_file, database):

    # TODO: Fix connection to sqlite
    os.environ['DB_NAME'] = database
    os.environ['TRYTOND_CONFIG'] = config_file

    # _CONFIG.current = config.set_trytond(config_file=config_file,
    #         database=database)
    _CONFIG.current = config.set_trytond()

    _CONFIG.current._context['language'] = os.getenv('TRYTON_LOCALE', 'en')
    _CONFIG.current._context['active_test'] = False
    return _CONFIG.current


def get_tryton_context():
    return _CONFIG.current._context


def init_logger(verbose):
    _logger.setLevel(logging.NOTSET)
    ch = logging.StreamHandler(sys.stdout)
    if not verbose:
        ch.setLevel(logging.ERROR)
    else:
        ch.setLevel(logging.INFO)
    formatter = logging.Formatter('%(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    _logger.addHandler(ch)
    return _logger


def get_logger():
    return _logger
