# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


def load_product_template(record, template):
    if record.get('code', None):
        template.products[0].code = record['code']

    return template
